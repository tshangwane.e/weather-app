export function fetchUserLocation() {
    return dispatch => {
        let options = {
            enableHighAccuracy: true,
            timeout: 5000,
            maximumAge: 0
        };
        const sucessCallBack = setLocation.bind(this,dispatch);
        const errorCallBack = setErrorRequestingLocation.bind(this,dispatch);

        setLocationRequest(dispatch);

        navigator.geolocation.getCurrentPosition(sucessCallBack, errorCallBack, options);
    };       
};

const setLocation = (dispatch, position ) => {
    dispatch({
        type: 'SET_LOCATION',
        payload: position.coords
    });  
 }

const setErrorRequestingLocation = (dispatch) => {
    dispatch({
        type: 'ERROR_REQUESTING_LOCATION'
    });  
 }

const setLocationRequest = (dispatch) => {
    dispatch({
        type: 'REQUESTING_LOCATION'
    });  
 }