import axios from 'axios';

export function fetchWeather(weather) {
    return dispatch =>  {
        const latitude = weather.latitude;
        const longitude = weather.longitude;
        const apiKey = "53f9d8e4213222cf517d86dc406d67fc";
        const apiBaseUrl = "https://api.openweathermap.org";
        const requestUrl = apiBaseUrl+'/data/2.5/weather?lat='+ latitude+'&lon='+longitude+'&appid='+apiKey;
        
        const successCallback =  setCurrentWeather.bind(this,dispatch);
        const errorCallback =  setErrorRequestingWeather.bind(this, dispatch);

        setWeatherRequest(dispatch);

        axios.get(requestUrl)
        .then(response => successCallback(response.data))   
        .catch(() => errorCallback());
        
    };
}; 

const setWeatherRequest = (dispatch) => {
    dispatch({
        type: 'REQUESTING_WEATHER'
    });
 }

 const setErrorRequestingWeather = (dispatch) => {
    dispatch({
        type: 'ERROR_REQUESTING_WEATHER'
    });
 }

 const setCurrentWeather = (dispatch, data) => {
    dispatch({
        type: 'SET_WEATHER',
        payload: data
    });   
 }