var INITIAL_STATE = {
    location:"", 
    dataStatus:"initial"
};

const locationReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'SET_LOCATION':
          return  {location : action.payload, dataStatus:'success'};
        case 'ERROR_REQUESTING_LOCATION':
            return {errorMsg:"Error occured while fetch your location.",dataStatus:'error'};
        case 'REQUESTING_LOCATION':
            return {requestMsg:'Fetching your location...',dataStatus:'requesting'};
        default:
          return state;
      }
    }
export default locationReducer;
    