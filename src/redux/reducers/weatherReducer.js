var INITIAL_STATE = {
    weather:"", 
    dataStatus:"initial"
};

const weatherReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'SET_WEATHER':
          return  {weather : action.payload, dataStatus:'success'};
        case 'ERROR_REQUESTING_WEATHER':
            return {errorMsg:'Error occurred while fetching your weather',dataStatus:'error'};
        case 'REQUESTING_WEATHER':
            return {requestMsg:'Fetching your weather...',dataStatus:'requesting'};
        default:
          return state;
      }
    }
export default weatherReducer;
    