import locationReducer from './locationReducer';
import weatherReducer from './weatherReducer';
import {combineReducers} from 'redux';

const rootReducer = combineReducers({locationReducer: locationReducer, weatherReducer: weatherReducer})

export default rootReducer;