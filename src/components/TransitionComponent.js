import {Col,Row,Alert,Spinner,Container, Card} from 'react-bootstrap';
import React from 'react';

function TransitionComponent(props) {
  return (
    <Container  fluid="md">
        <Row className="justify-content-md-center">
            <Col xs={3}>
            <Card>
                <Card.Header>Please wait</Card.Header>
                    <Card.Body>
                        <Alert variant="info">
                        <Spinner animation="border" role="status">
                        </Spinner>
                        <div className="sr-only">{props.requestMsg}</div>
                </Alert>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    </Container>  
  )
}
export default TransitionComponent