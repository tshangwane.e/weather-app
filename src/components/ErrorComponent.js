
import {Col,Row,Alert,Container, Card} from 'react-bootstrap';
import React from 'react';

function ErrorComponent(props) {
  return (
        <Container  fluid="md">
            <Row className="justify-content-md-center">
                <Col xs={3}>
                    <Card>
                        <Card.Header>Error</Card.Header>
                        <Card.Body>
                            <Alert variant="danger">
                            <div className="sr-only">{props.errorMsg}</div>
                        </Alert>
                        </Card.Body>
                    </Card>
                </Col>
                </Row>
        </Container>
  )
}
export default ErrorComponent