import React from 'react';
import {Col,Row,Button,Container, Card, Image} from 'react-bootstrap';

function SuccessComponent(props) {
  return (
        <Container  fluid="md">
            <Row className="justify-content-md-center">
                <Col xs={3}>
                    <Card>
                        <Card.Header>{props.weather.name}</Card.Header>
                        <Card.Body className="succes-background">
                        <Card.Title>{props.weather.weather[0].description.toUpperCase()}</Card.Title>
                        <Card.Text>
                            <Image src={`http://openweathermap.org/img/wn/${props.weather.weather[0].icon}.png`} rounded />
                        </Card.Text>
                        <Card.Text className="temp">
                            Temperature :  {props.weather.main.temp} F 
                        </Card.Text>
                        <Button variant="primary" onClick={props.refresh}>Refresh</Button>
                        </Card.Body>
                    </Card>
                </Col>
                </Row>
            </Container>
  )
}
export default SuccessComponent