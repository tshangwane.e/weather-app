import React, {useEffect} from 'react';
import { connect } from "react-redux";
import {fetchWeather} from "../redux/actions/weatherActions";
import ErrorComponent from './ErrorComponent';
import TransitionComponent from './TransitionComponent';
import SuccessComponent from './SuccessComponent';

function Weather(props) {
  
  useEffect(() => {props.fetchWeather(props.userLocation)},[]);
  const onRefresh = ()=>{props.refreshData();};
  return (
     props.dataStatus == "requesting"? 
      <TransitionComponent requestMsg = {props.requestMsg} /> 
    :
    props.dataStatus == "success"?
      <SuccessComponent weather={props.weather} refresh={onRefresh}/>
    :
      <ErrorComponent errorMsg = {props.errorMsg}/>
  )
  
}

const mapDispatchToProps = (dispatch)=>{
  return {
    fetchWeather : (props)=>{ 
      return dispatch(fetchWeather(props))
    }
  }
}
const mapStateToProps = (state)=>{
  return state.weatherReducer;
}

export default connect(mapStateToProps, mapDispatchToProps)(Weather)