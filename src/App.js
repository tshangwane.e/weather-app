import './App.css';
import { connect } from "react-redux";
import {fetchUserLocation} from "./redux/actions/locationActions";
import React, {useEffect } from 'react';
import Weather from './components/Weather';
import ErrorComponent from './components/ErrorComponent';
import TransitionComponent from './components/TransitionComponent';

function App(props) {
  if(navigator.geolocation){
    useEffect(() => {props.fetchLocation()},[]);  
  }
 return (
    props.dataStatus == "requesting" ? 
      <TransitionComponent requestMsg = {props.requestMsg} /> 
    : 
    props.dataStatus == "error" ? 
      <ErrorComponent errorMsg = {props.errorMsg}/>
    :
    props.dataStatus == "success" ? 
      <Weather userLocation={props.location} refreshData={props.fetchLocation}/>
    :
      <ErrorComponent errorMsg = {"Sorry, your browser does not support geolocation."}/>
  );  
}

const mapDispatchToProps = (dispatch)=>{
  return {
    fetchLocation : (props)=>{ 
      return dispatch(fetchUserLocation(props))
    }
  }
}

const mapStateToProps = (state)=>{
  return state.locationReducer;
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
