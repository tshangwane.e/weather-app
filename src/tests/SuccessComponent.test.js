import React from 'react';
import { act } from "react-dom/test-utils";
import { render, unmountComponentAtNode } from "react-dom";
import  SuccessComponent from '../components/SuccessComponent';
import { Simulate } from 'react-dom/test-utils';
let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

test('test displays correct weather details', () => {
    const onChange = jest.fn();
    const mockWeather = {
        name:"Gauteng",
        weather :[
            {
                description:"Sunny",
                icon:"sun"
            }
        ],
        main: {
            temp:200
        }
    }
    
    act(() => {
        render(<SuccessComponent weather={mockWeather} refresh={onChange}/>, container);
    });

    expect(container.querySelector('.card-header').textContent).toBe("Gauteng");
    expect(container.querySelector('.card-title').textContent).toBe("SUNNY");
    expect(container.querySelector('.rounded').getAttribute("src")).toEqual("http://openweathermap.org/img/wn/sun.png");
    expect(container.querySelector('.temp').textContent).toBe("Temperature :  200 F"); 
});

test('test onClick calls the correct function', () => {    
    const onChange = jest.fn();
    const mockWeather = {
        name:"Gauteng",
        weather :[
            {
                description:"Sunny",
                icon:"sun"
            }
        ],
        main: {
            temp:200
        }
    }
   
    act(() => {
        render(<SuccessComponent weather={mockWeather} refresh={onChange}/>, container);
    });
    
    const button = container.querySelector("button");
    
    act(() => {
       button.dispatchEvent(new MouseEvent("click", { bubbles: true }));
    });
    
      expect(onChange).toHaveBeenCalledTimes(1);
  });