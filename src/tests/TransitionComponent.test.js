import React from 'react';
import { act } from "react-dom/test-utils";
import { render, unmountComponentAtNode } from "react-dom";
import  TransitionComponent from '../components/TransitionComponent';

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

test('test correctly sets transition message', () => {
  act(() => {
    render(<TransitionComponent requestMsg="Requesting weather at your location"/>, container);
  });

  expect(container.querySelector('.sr-only').textContent).toBe("Requesting weather at your location");
});

test('test has correct header text', () => {
    act(() => {
      render(<TransitionComponent requestMsg="Requesting location"/>, container);
    });
  
    expect(container.querySelector('.card-header').textContent).toBe("Please wait");
  });
  