import React from 'react';
import { act } from "react-dom/test-utils";
import { render, unmountComponentAtNode } from "react-dom";
import  ErrorComponent from '../components/ErrorComponent';

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

test('test correctly sets error message', () => {
  act(() => {
    render(<ErrorComponent errorMsg="Error occurred while proccessing"/>, container);
  });

  expect(container.querySelector('.sr-only').textContent).toBe("Error occurred while proccessing");
});

test('test has correct header text', () => {
    act(() => {
      render(<ErrorComponent errorMsg="Error"/>, container);
    });
  
    expect(container.querySelector('.card-header').textContent).toBe("Error");
  });
  
